const restify = require("restify");
const metatags = require("./metatags")();
const colors = require("colors");
const cacheManager = require("cache-manager");
const redisStore = require("cache-manager-redis");
const redisCache = cacheManager.caching({
  store: redisStore,
  host: process.env.REDIS_HOST || "localhost",
  port: process.env.REDIS_PORT || 6379,
  db: 0,
  ttl: process.env.CACHE_TTL || 24 * 60 * 60
});

const server = restify.createServer({
  name: "Unipaper Metadata Server"
});

server.get("/meta/:contentType/:identifier", (req, res, next) => {
  var contentType = req.params.contentType;
  var identifier = encodeURIComponent(req.params.identifier);
  console.log(`getting /meta/${contentType}/${identifier}`.dim);
  redisCache.wrap(
    `${contentType}/${identifier}`,
    cb => {
      metatags.getMetaData(contentType, identifier).then(
        metaTags => {
          console.log(
            `fetched data from contentful for ${contentType}/${identifier}`
              .yellow
          );
          cb(null, metaTags);
        },
        () => {
          cb(true);
        }
      );
    },
    {},
    (err, metaTags) => {
      if (err) {
        console.log(`failed /meta/${contentType}/${identifier}`.red);
        res.status(404, "Page not found");
        res.end();
        return next();
      }

      console.log(`sent /meta/${contentType}/${identifier}`.green);
      res.writeHead(200, {
        "Content-Type": "text/html"
      });
      res.write(metaTags);
      res.end();
      return next();
    }
  );
});

server.listen(process.env.PORT || 8080, () => {
  console.log(`${server.name} listening on port ${server.url}`.blue);
});
