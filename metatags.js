const https = require("https");
const Promise = require("promise");

const ACCESS_TOKEN =
  process.env.CONTENTFUL_DELIVERY_ACCESS_TOKEN ||
  "71350235e5e038b924002905ef9fd074fd07f8798b09f098e66b6d1fc03a7960";
const SPACE_ID = process.env.CONTENTFUL_SPACE_ID || "6n4dchzgpcdt";
const DOMAIN_NAME = process.env.DOMAIN_NAME || "https://www.unipaper.co.uk";
const FB_APPID = process.env.FB_APPID || "1404174076277552";

var getImage = body => {
  var aStory = body.items[0];
  var assets = body.includes.Asset;
  var sysId = "";
  var imageUrl = "";
  if (aStory.sys.contentType !== undefined) {
    var contentType = aStory.sys.contentType.sys.id;
    switch (contentType) {
      case "article":
      case "listicle":
      case "gallery":
        sysId =
          aStory.fields !== undefined &&
          aStory.fields[contentType + "Image"] !== undefined &&
          aStory.fields[contentType + "Image"].sys !== undefined &&
          aStory.fields[contentType + "Image"].sys.id !== undefined
            ? aStory.fields[contentType + "Image"].sys.id
            : "";
        break;
    }
    if (sysId !== "") {
      for (var i = 0; i < assets.length; i++) {
        if (assets[i].sys.id === sysId) {
          imageUrl = assets[i].fields.file.url;
          imageUrl =
            imageUrl !== "" && imageUrl.substr(0, 3) !== "http"
              ? "http:" + imageUrl
              : imageUrl;
          break;
        }
      }
    }
    return imageUrl;
  } else {
    return "";
  }
};

var formMetaTagsFromStoryInfo = function(aStory) {
  var metaTags = {
    title:
      aStory.title !== ""
        ? aStory.title + " | The University Paper"
        : "The University Paper",
    ogUrl: DOMAIN_NAME + "/" + aStory.contentType + "/" + aStory.slug,
    ogImage:
      aStory.image !== ""
        ? aStory.image
        : "http://images.contentful.com/17u05p4x0l0r/1nuBvnXtgECCuWaKKWWuSc/5ecba7f23e8f1f31382e457e8b667c07/Rogue_One_-_A_Star_Wars_Story.jpg",
    description: aStory.title + aStory.subHeading
  };
  return (
    '<meta property="description" content="' +
    metaTags.description +
    '" /><meta property="og:title" content="' +
    metaTags.title +
    '" /><meta property="og:type" content="website" /><meta property="og:url" content="' +
    metaTags.ogUrl +
    '" /><meta property="og:image" content="' +
    metaTags.ogImage +
    '" /><meta property="og:description" content="' +
    metaTags.description +
    '" /><meta property="fb:app_id" content="' +
    FB_APPID +
    '" />'
  );
};

var fetchFromContentful = (contentType, identifier, field) => {
  var promise = new Promise(function(resolve, reject) {
    var options = {
      hostname: "cdn.contentful.com",
      port: 443,
      path: `/spaces/${SPACE_ID}/entries?access_token=${ACCESS_TOKEN}&content_type=${contentType}&${field}=${identifier}&include=3`,
      method: "GET"
    };
    var req = https.request(options, function(res) {
      var body = "";
      res.on("data", function(chunk) {
        body += chunk;
      });

      res.on("end", function() {
        body = JSON.parse(body);
        var storyRaw = body.items[0];
        if (storyRaw !== undefined) {
          var story = {
            id: storyRaw.sys.id,
            title: storyRaw.fields.title,
            contentType:
              storyRaw.sys.contentType !== undefined
                ? storyRaw.sys.contentType.sys.id
                : "",
            subHeading:
              storyRaw.fields.subHeading !== undefined
                ? storyRaw.fields.subHeading
                : "",
            image: getImage(body),
            slug:
              storyRaw.fields !== undefined &&
              storyRaw.fields.urlSlug !== undefined &&
              storyRaw.fields.urlSlug !== ""
                ? storyRaw.fields.urlSlug
                : storyRaw.sys.id
          };
          var metaTags = formMetaTagsFromStoryInfo(story);
          resolve(metaTags);
        } else {
          reject();
        }
      });
    });

    req.on("error", function(err) {
      process.stderr.write(err);
      reject();
    });

    req.end();
  });

  return promise;
};

module.exports = () => {
  var MetaTags = {};

  MetaTags.getMetaData = function(contentType, identifier) {
    var promise = new Promise((resolve, reject) => {
      var field = identifier.indexOf("-") > 0 ? "fields.urlSlug" : "sys.id";
      var secondaryField =
        field === "fields.urlSlug" ? "sys.id" : "fields.urlSlug";

      fetchFromContentful(contentType, identifier, field).then(
        metaTags => {
          resolve(metaTags);
        },
        error => {
          fetchFromContentful(contentType, identifier, secondaryField).then(
            metaTags => {
              resolve(metaTags);
            },
            error => {
              reject();
            }
          );
        }
      );
    });

    return promise;
  };

  return MetaTags;
};
