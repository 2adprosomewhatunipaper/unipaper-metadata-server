# unipaper-metadata-server
A restify server that gets article information from contentful and returns it as meta tags for facebook bots. For all other known bots we use a fork of [Prerender.io's open source Github project](https://github.com/prerender/prerender).

When a page is shared on facebook, 
1. the facebook bot **facebookexternalhit** makes a request to the page
2. the user portal server redirects the request to the prerender server as <METADATA_SERVER_URL>/meta/article/<ARTICLE_IDENTIFIER>.
3. the route /meta/article/<ARTICLE_IDENTIFIER> fetches article information using the identifier and embeds it into meta tags like title, og:image, description etc. and returns the meta data to the facebook bot.

## Set Up
### Installation
Clone this repo and run `npm install` to install dependencies and packages.

To test prerendering efficiently for the Unipaper user portal, you must set up [unipaper-prerender](https://bitbucket.org/2adprosomewhatunipaper/unipaper-prerender) (Check README.md for installation and set up) for all bots except Facebook and this project for the Facebook bot. 
Once set up add this sample .htaccess file to your User portal project folder. Replace <PRERENDER_SERVER_URL>, <USER_PORTAL_URL> and <METADATA_SERVER_URL> with the corresponding values for your setup.

```
# Change YOUR_TOKEN to your prerender token and uncomment that line if you want to cache urls and view crawl stats
# Change http://example.com (at the end of the last RewriteRule) to your website url

<IfModule mod_headers.c>
    #RequestHeader set X-Prerender-Token "YOUR_TOKEN"
</IfModule>

<IfModule mod_rewrite.c>
    RewriteEngine On

    <IfModule mod_proxy_http.c>
      RewriteCond %{HTTP_USER_AGENT} baiduspider|twitterbot|rogerbot|linkedinbot|embedly|quora\ link\ preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator [NC,OR]
      RewriteCond %{QUERY_STRING} _escaped_fragment_

      # Only proxy the request to Prerender if it's a request for HTML
      RewriteRule ^(.*) <PRERENDER_SERVER_URL>/<USER_PORTAL_URL>/$1 [P,L]
    </IfModule>

    <IfModule mod_proxy_http.c>
      RewriteCond %{HTTP_USER_AGENT} facebookexternalhit [NC,OR]
      RewriteCond %{QUERY_STRING} _escaped_fragment_

      # Only proxy the request to Prerender if it's a request for HTML
      RewriteRule ^(.*) <METADATA_SERVER_URL>/meta/$1 [P,L]
    </IfModule>

    RewriteCond %{REQUEST_FILENAME} -s [OR]
    RewriteCond %{REQUEST_FILENAME} -l [OR]
    RewriteCond %{REQUEST_FILENAME} -d
    RewriteRule ^.*$ - [NC,L]

    RewriteRule ^(.*) index.html [NC,L]
</IfModule>
```

### Environment variables
* CONTENTFUL_DELIVERY_ACCESS_TOKEN - Contentful delivery access token (required)
* CONTENTFUL_SPACE_ID - Contentful space id (required)
* DOMAIN_NAME - Domain name for the environment (required)
* FB_APPID (required)
* PORT - Defaults to 8080
* REDIS_HOST, REDIS_PORT, CACHE_TTL - Redis config

## Run
`node server.js`

To impersonate a facebook bot you can use cURL

`curl -A facebookexternalhit <USER_PORTAL_LINK_TO_SHARE>`